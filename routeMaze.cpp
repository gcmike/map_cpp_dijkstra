/*******************************************************************************
** This is an exercise for the application of the position of associate software
** engineer in iRobot. Input files are in JSON formatted *.dat with start point,
** end point, and obstacle points of a maze clearly indicated. The task is to go
** from start to end effectively.
**
** This program is based on the fact that if the grid next to start point can
** reach the end, so can the start. Hence the algorithm recursively labels the
** grid in a map with the distance to the start point. Once the end point is
** reached, there will be a path labeled with increasing integers from the start
** to the end.
**
** Auther: Yen-Ming Huang
** Date: September 15, 2013
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <queue>
#include <stack>

using namespace std;

enum MAP_LEBEL{
     START = 0,
     END = -10,
     OBJ = -5,
     EMTY = -1
     };     

// Class representing coordinate points
class grid{
      public:
             grid(int ir, int ic){row = ir; col = ic;}
             grid(){row = -1; col = -1;}
             int getr()const {return row;}
             int getc()const {return col;}
             int setr(int rr) {row = rr;}
             int setc(int cc) {col = cc;}
             bool isgreaterr(int gr) {return row > gr;}
             bool isgreaterc(int gc) {return col > gc;}
      private:
              int row;
              int col;
      };

void getGrid(string, grid&, grid&, queue<grid>&);
grid getMax(grid&, grid&, queue<grid>);
void buildMap(queue<grid>, grid&, grid&, grid&, double**&);
void label(double**&, grid&, grid&, queue<grid>&);
bool planRoute(double**&, queue<grid>&, grid&, grid&);
void traceBack(double**&, grid&, stack<grid>&);

int main(int argc, char** argv){
    grid st, en, max; // start point, end point, and maximum grid values
    queue<grid> obs; // queue of obstacles
    double** map; // map of the maze
    string filename("set6.dat");
    
    getGrid(filename, st, en, obs); // parse the input file
    max = getMax(st, en, obs); // get maximum grid values for creating a map
    buildMap(obs, st, en, max, map);
    
    queue<grid> init; // queue for the start of route-planning
    stack<grid> road; // stack of the final route
    init.push(st);
    if(planRoute(map, init, en, max)){
        cout<<"Solution found."<<endl;
        road.push(en);
        }
    else
        cout<<"No solution."<<endl;
    
    traceBack(map, en, road);
    
    // Output
    ofstream ff("set6.txt");
    if(ff.is_open()){
        // output the map after labeling
        /*for(int i = 0 ; i < max.getr()+1; i++){
                for(int j = 0; j < max.getc()+1; j++)
                        ff<<map[i][j]<<",";
                ff<<endl;
                }*/
        
        // output desired
        if(road.empty())
            ff<<"No solution."<<endl;
        else
            while(!road.empty()){
                ff<<road.top().getr()<<","<<road.top().getc()<<endl;
                road.pop();
                }
      }
    ff.close();
    
    // Memory release
    for(int i = 0; i < max.getr()+1; i++)
            delete [] map[i];
    delete [] map;
    
    system("pause");
    return 0;
}

// getGrid takes in the filename and parses to output a start grid, an end grid,
// and a queue of obstacles.
void getGrid(string filename, grid& st, grid& en, queue<grid>& obs){
     ifstream infile;
     infile.open(filename.c_str());
     string inputtmp, argument[3];
     queue<string> instring;
     while(getline(infile,inputtmp,'"'))
         instring.push(inputtmp);
     for(int i = 0; i < 3; i++){
             instring.pop();
             instring.pop();
             argument[i] = instring.front();
             }
     char* buf = strdup(argument[0].c_str());
     char* tmpstr;
     grid tmpgrid;
     tmpstr = strtok(buf," :{}[],");
     do{
         tmpgrid.setr(atoi(tmpstr));
         tmpstr = strtok(NULL, " :{}[],");
         tmpgrid.setc(atoi(tmpstr));
         obs.push(tmpgrid);
         tmpstr = strtok(NULL, " :{}[],");
         }while(tmpstr != NULL);
     
     buf = strdup(argument[1].c_str());
     tmpstr = strtok(buf," :{}[],");
     st.setr(atoi(tmpstr));
     tmpstr = strtok(NULL, " :{}[],");
     st.setc(atoi(tmpstr));
     
     buf = strdup(argument[2].c_str());
     tmpstr = strtok(buf," :{}[],");
     en.setr(atoi(tmpstr));
     tmpstr = strtok(NULL, " :{}[],");
     en.setc(atoi(tmpstr));
     }

// Find the maximum x and y for creating a map.
grid getMax(grid &st, grid &en, queue<grid> obs){
     grid max(0,0);
     while(!obs.empty()){
         if(obs.front().isgreaterr(max.getr()))
             max.setr(obs.front().getr());
         if(obs.front().isgreaterc(max.getc()))
             max.setc(obs.front().getc());
         obs.pop();
         }
     if(st.isgreaterr(max.getr()))
         max.setr(st.getr());
     if(st.isgreaterc(max.getc()))
         max.setc(st.getc());
     if(en.isgreaterr(max.getr()))
         max.setr(en.getr());
     if(en.isgreaterc(max.getc()))
         max.setc(en.getc());
     return max;
     }

// Build a map with OBJ representing obstacles, START representing the start,
// END representing the end point, and EMTY representing any of the others.
void buildMap(queue<grid> obs, grid &st, grid &en, grid &max, double** &map){
     map = new double* [max.getr()+1];
     for(int i = 0; i < max.getr()+1; i++)
             map[i] = new double[max.getc()+1];
     for(int i = 0; i < max.getr()+1; i++)
             for(int j = 0; j < max.getc()+1; j++)
                     map[i][j] = EMTY;
     
     while(!obs.empty()){
         map[obs.front().getr()][obs.front().getc()] = OBJ;
         obs.pop();
         }
     map[st.getr()][st.getc()]=START;
     map[en.getr()][en.getc()]=END;
     }

// Labels the 4 neighbors with 1 distance more from the start poing than the 
// current one if they are eligible to be a route. Again, OBJ is a obstacle,
// EMTY is eligible route, and END is the end point, which also needs to be
// labeled. Queue levelgrid is for storing the newly labeled ones for the next 
// recursive call.
void label(double** &map, grid &now, grid &max, queue<grid> &levelgrid){
     double level = map[now.getr()][now.getc()];
     // label up, hence checking if it is at the top row
     if(now.getr() != 1&& map[now.getr()-1][now.getc()] != OBJ){
         if(map[now.getr()-1][now.getc()]==EMTY || 
                             map[now.getr()-1][now.getc()]==END || 
                             map[now.getr()-1][now.getc()]>level+1){
             map[now.getr()-1][now.getc()] = level + 1;
             grid tmp(now.getr()-1,now.getc());
             levelgrid.push(tmp);
             }
         }
     // label down, hence checking if it is at the bottom row
     if(now.getr() != max.getr() && map[now.getr()+1][now.getc()] != OBJ){
         if(map[now.getr()+1][now.getc()]==EMTY || 
                             map[now.getr()+1][now.getc()]==EMTY || 
                             map[now.getr()+1][now.getc()]>level+1){
             map[now.getr()+1][now.getc()] = level + 1;
             grid tmp(now.getr()+1,now.getc());
             levelgrid.push(tmp);
             }
         }
     // label left, hence chekcing if it is at the first column
     if(now.getc() != 1 && map[now.getr()][now.getc()-1] != OBJ){
         if(map[now.getr()][now.getc()-1]==EMTY || 
                             map[now.getr()][now.getc()-1]==END ||  
                             map[now.getr()][now.getc()-1]>level+1){
             map[now.getr()][now.getc()-1] = level + 1;
             grid tmp(now.getr(),now.getc()-1);
             levelgrid.push(tmp);
             }
         }
     // label right, hence checking if it is at the last column
     if(now.getc() != max.getc() && map[now.getr()][now.getc()+1] != OBJ){
         if(map[now.getr()][now.getc()+1]==EMTY || 
                             map[now.getr()][now.getc()+1]==END ||  
                             map[now.getr()][now.getc()+1]>level+1){
             map[now.getr()][now.getc()+1] = level + 1;
             grid tmp(now.getr(),now.getc()+1);
             levelgrid.push(tmp);
             }
         }
     }

// Recursively call this part to label the map with integers increasing sequen-
// tially. Queue next represents the grids that are labeled in this step and 
// will be used as the labeling queue for the next recursive call. In the end, 
// return 1 if one of the point in the queue reaches the end point, and return
// 0 if the end point has not been reached and there are no grids to be labeled.
bool planRoute(double** &map, queue<grid> &now, grid &en, grid &max){
     queue<grid> next;
     while(!now.empty()){
         if(now.front().getr()==en.getr() && now.front().getc()==en.getc())
             return 1;
         else{
             label(map, now.front(), max, next);
             now.pop();
             }
         }
     if(next.empty())
         return 0;
     else if(planRoute(map, next, en, max))
         return 1;
     else
         return 0;
     /*if(planRoute(map, next, en, max))
         return 1;
     else
         return 0;*/
     }

// Starting from the end point, check the 4 neighboring grid on the map for a
// sequential decrement route back to the start point, and push the route into
// a stack for output.
void traceBack(double** &map, grid &now, stack<grid> &road){
     if(map[now.getr()][now.getc()]!=START){
         if(map[now.getr()][now.getc()]-map[now.getr()-1][now.getc()] ==1){
             grid tmpback(now.getr()-1,now.getc());
             road.push(tmpback);
             traceBack(map, tmpback, road);
             }
         else if(map[now.getr()][now.getc()]-map[now.getr()+1][now.getc()] ==1){
             grid tmpback(now.getr()+1,now.getc());
             road.push(tmpback);
             traceBack(map, tmpback, road);
             }
         else if(map[now.getr()][now.getc()]-map[now.getr()][now.getc()-1] ==1){
             grid tmpback(now.getr(),now.getc()-1);
             road.push(tmpback);
             traceBack(map, tmpback, road);
             }
         else if(map[now.getr()][now.getc()]-map[now.getr()][now.getc()+1] ==1){
             grid tmpback(now.getr(),now.getc()+1);
             road.push(tmpback);
             traceBack(map, tmpback, road);
             }
         }
     }
